import {
  showUserInfor,
  getInforFromForm,
  showUserInforToForm,
} from "./controllers/controller.js";
import { Validation } from "./models/studentModel.js";
import { BASE_URL } from "./services/studentServices.js";

let turnOnLoading = () => {
  document.querySelector("#loading").style.display = "flex";
};
let turnOffLoading = () => {
  document.querySelector("#loading").style.display = "none";
};
let validation = new Validation();
let renderUsersList = () => {
  turnOnLoading();
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      showUserInfor(res.data);
    })
    .catch((err) => {
      console.log("Error");
    });
};
renderUsersList();
document.querySelector("#btnThemNguoiDung").addEventListener("click", () => {
  document.querySelector("#add_user").style.display = "block";
  document.querySelector("#update_user").style.display = "none";
  document.querySelector("#id-user").style.display = "none";
  document.querySelector("#modal-form").reset();
  document.querySelector("#TaiKhoan").disabled = false;
});
// Validation

let checkValidation = (para) => {
  let isValidAccount = para && validation.checkEmpty("#TaiKhoan", "#tbaccount");
  let isValidName =
    validation.checkEmpty("#HoTen", "#fullNametb", "Vui lòng nhập Họ tên") &&
    validation.checkFullName();

  let isValidPassword = validation.checkPassword();
  let isValidEmail = validation.checkEmail();
  let isValidImage = validation.checkEmpty("#HinhAnh", "#imgtb");
  let isValidType = validation.checkOption("#loaiNguoiDung", "#typetb");
  let isValidLanguage = validation.checkOption("#loaiNgonNgu", "#languagetb");
  let isValidDescription =
    validation.checkEmpty("#MoTa", "#destb") && validation.checkDescription();

  return (
    isValidAccount &&
    isValidName &&
    isValidPassword &&
    isValidEmail &&
    isValidImage &&
    isValidType &&
    isValidLanguage &&
    isValidDescription
  );
};

document.querySelector("#add_user").addEventListener("click", () => {
  turnOnLoading;

  let checkDuplicateServ = () => {
    let inputValue = document.querySelector("#TaiKhoan").value;
    axios({
      url: `${BASE_URL}`,
      method: "GET",
    })
      .then((res) => {
        const result = res.data;
        let position = result.findIndex((item) => {
          return item.account == inputValue;
        });
        const checkDup = validation.checkDuplicate(position);
        let isValid = checkValidation(checkDup);
        let newUsersInfor = getInforFromForm();
        if (isValid) {
          axios({
            url: `${BASE_URL}`,
            method: "POST",
            data: newUsersInfor,
          })
            .then((res) => {
              $("#myModal").modal("hide");
              turnOffLoading();
              renderUsersList();
            })
            .catch((err) => {
              turnOffLoading();
              console.log("Error");
            });
        }
      })
      .catch((err) => {
        console.log("Error");
      });
  };
  checkDuplicateServ();
});

// delete user
let deleteUser = (id) => {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      turnOffLoading();
      renderUsersList();
    })
    .catch((err) => {
      turnOffLoading();
      console.log("Error");
    });
};
window.deleteUser = deleteUser;

// Edit User Infor

let editUser = (id) => {
  document.querySelector("#TaiKhoan").disabled = true;
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      showUserInforToForm(res.data);
      document.querySelector("#update_user").style.display = "block";
      document.querySelector("#add_user").style.display = "none";
      document.querySelector("#id-user span").innerHTML = `${id}`;
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
};
window.editUser = editUser;

document.querySelector("#update_user").addEventListener("click", () => {
  let idusers = document.querySelector("#id-user span").innerHTML * 1;
  turnOnLoading();
  checkValidation();
  let isValid = checkValidation(true);
  if (isValid) {
    let editedUser = getInforFromForm();

    axios({
      url: `${BASE_URL}/${idusers}`,
      method: "PUT",
      data: editedUser,
    })
      .then((res) => {
        $("#myModal").modal("hide");
        turnOffLoading();
        renderUsersList();
      })
      .catch((err) => {
        turnOffLoading();
        console.log("Error");
      });
  }
});

// Search User Name
document.querySelector("#search_name").addEventListener("focus", () => {
  renderUsersList();
});
document.querySelector("#basic-addon2").addEventListener("click", () => {
  turnOnLoading();
  let searchValue = document.querySelector("#search_name").value;
  console.log(searchValue);
  if (searchValue != "") {
    let searchList = [];
    axios({
      url: `${BASE_URL}`,
      method: "GET",
    })
      .then((res) => {
        searchList = res.data.filter((item) => {
          if (item.fullName == searchValue) {
            return item;
          }
        });
        showUserInfor(searchList);
        turnOffLoading();
      })
      .catch((err) => {
        turnOffLoading();
        console.log("Error");
      });
  } else {
    renderUsersList();
  }
});
