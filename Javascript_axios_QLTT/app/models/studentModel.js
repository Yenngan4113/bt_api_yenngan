import { BASE_URL } from "../services/studentServices.js";

class Account {
  constructor(
    _account,
    _fullName,
    _passWord,
    _email,
    _img,
    _type,
    _language,
    _description
    // ...id
  ) {
    this.account = _account;
    this.fullName = _fullName;
    this.password = _passWord;
    this.email = _email;
    this.img = _img;
    this.type = _type;
    this.language = _language;
    this.description = _description;
    // this.id = id;
  }
}

export { Account };

class Validation {
  constructor() {
    this.checkEmpty = (idCheck, idError, messageError) => {
      let inputValue = document.querySelector(idCheck).value;
      if (inputValue) {
        document.querySelector(idError).innerText = "";
        return true;
      } else {
        document.querySelector(idError).innerText = "Vui lòng không để trống";
        return false;
      }
    };
    this.checkDuplicate = (viTri) => {
      if (viTri == -1) {
        document.querySelector("#tbaccount").innerText = "";
        return true;
      } else {
        console.log("no");
        document.querySelector("#tbaccount").innerText = "Account bị trùng";
        return false;
      }
    };
    this.checkFullName = () => {
      let inputValue = document.querySelector("#HoTen").value;

      let letters = /^[a-zA-Z\s]*$/;
      if (letters.test(inputValue)) {
        document.querySelector("#fullNametb").innerText = "";
        return true;
      } else {
        document.querySelector("#fullNametb").innerText = "Tên không hợp lệ";
      }
    };
    this.checkPassword = () => {
      let inputValue = document.querySelector("#MatKhau").value;
      let pass = /^(?=.*?[0-9])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\w\W]{6,8}$/;
      if (inputValue.match(pass)) {
        document.querySelector("#passtb").innerText = "";
        return true;
      } else {
        document.querySelector("#passtb").innerText =
          "Password phải có từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
        return false;
      }
    };
    this.checkEmail = () => {
      let inputValue = document.querySelector("#Email").value;
      let checkemail =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
      if (inputValue.match(checkemail)) {
        document.querySelector("#emailtb").innerText = "";
        return true;
      } else {
        document.querySelector("#emailtb").innerText = "Email không hợp lệ";
        return false;
      }
    };
    this.checkOption = (idCheck, idError) => {
      let inputValue = document.querySelector(idCheck).value;
      if (inputValue == "10" || inputValue == "") {
        document.querySelector(idError).innerText = "Vui lòng chọn";
        return false;
      } else {
        document.querySelector(idError).innerText = "";
        return true;
      }
    };
    this.checkDescription = () => {
      let inputValue = document.querySelector("#MoTa").value;
      if (inputValue.length <= 60) {
        document.querySelector("#destb").innerText = "";
        return true;
      } else {
        document.querySelector("#destb").innerText = "Mô tả ít hơn 60 ký tự";
        return false;
      }
    };
  }
}

export { Validation };
