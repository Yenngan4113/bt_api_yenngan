import { Account } from "../models/studentModel.js";

let showUserInfor = (user) => {
  let contentHTML = "";
  user.forEach((item) => {
    let contentTrTag = /*html*/ `<tr>
      <td>${item.id}</td>
      <td>${item.account}</td>
      <td>${item.password}</td>
      <td>${item.fullName}</td>
      <td>${item.email}</td>
      <td>${item.language}</td>
      <td>${item.type ? "Giáo viên" : "Học Viên"}</td>
      <td>${item.description}</td>
      <td><button class="btn btn-success" data-toggle="modal" onClick="editUser('${
        item.id
      }')"
      data-target="#myModal"
      >Sửa</button>
      <button class="btn btn-danger" onClick="deleteUser('${
        item.id
      }')">Xóa</button></td>
      </tr>`;
    contentHTML += contentTrTag;
  });
  document.querySelector("#tblDanhSachNguoiDung").innerHTML = contentHTML;
};
export { showUserInfor };

let getInforFromForm = () => {
  let account = document.querySelector("#TaiKhoan").value;
  let fullName = document.querySelector("#HoTen").value;
  let password = document.querySelector("#MatKhau").value;
  let email = document.querySelector("#Email").value;
  let img = document.querySelector("#HinhAnh").value;
  let userType = document.querySelector("#loaiNguoiDung").value * 1;
  let language = document.querySelector("#loaiNgonNgu").value;
  let description = document.querySelector("#MoTa").value;
  let newUsers = new Account(
    account,
    fullName,
    password,
    email,
    img,
    userType == 1 ? true : false,
    language,
    description
  );
  return newUsers;
};
export { getInforFromForm };
let showUserInforToForm = (data) => {
  document.querySelector("#TaiKhoan").value = data.account;
  document.querySelector("#HoTen").value = data.fullName;
  document.querySelector("#MatKhau").value = data.password;
  document.querySelector("#Email").value = data.email;
  document.querySelector("#HinhAnh").value = data.img;
  document.querySelector("#loaiNguoiDung").value = data.type ? "1" : "0";
  document.querySelector("#loaiNgonNgu").value = data.language;
  document.querySelector("#MoTa").value = data.description;
};
export { showUserInforToForm };
