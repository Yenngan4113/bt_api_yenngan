let BASE_URL = "https://6271e18225fed8fcb5ec0cdd.mockapi.io/center-management";
class Teacher {
  constructor(
    _id,
    _account,
    _fullName,
    _password,
    _email,
    _type,
    _language,
    _description,
    _img
  ) {
    this.id = _id;
    this.account = _account;
    this.fullName = _fullName;
    this.password = _password;
    this.email = _email;
    this.type = _type;
    this.language = _language;
    this.description = _description;
    this.img = _img;
  }
}
let teacher1 = new Teacher(
  1,
  "jroberts",
  "July Roberts",
  "123456",
  "jroberts@gmail.com",
  true,
  "ITALIAN",
  "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
  "teacher_1.jpg"
);
let teacher2 = new Teacher(
  2,
  "jstone",
  "Jon Stone",
  "123456",
  "jstone@gmail.com",
  true,
  "FRENCH",
  "Nanotechnology immersion along the information.",
  "teacher_2.jpg"
);
let teacher3 = new Teacher(
  3,
  "ntran",
  "Nguyen Tran",
  "123456",
  "ntran@gmail.com",
  false,
  "CHINESE",
  "Nanotechnology immersion along the information.",
  "teacher_2.jpg"
);

let postUserInforServ = (data) => {
  axios({
    url: `${BASE_URL}`,
    method: "POST",
    data: data,
  });
};
let showUserInfor = (user) => {
  let contentHTML = "";
  user.forEach((item) => {
    if (item.type) {
      let contentTrTag = /*html*/ `<tr>
        <td>${item.id}</td>
        <td>${item.account}</td>
        <td>${item.password}</td>
        <td>${item.fullName}</td>
        <td>${item.email}</td>
        <td>${item.language}</td>
        <td>${item.type ? "Giáo viên" : "Học Viên"}</td>
        <td>${item.description}</td>
        </tr>`;
      contentHTML += contentTrTag;
    }
  });
  document.querySelector("#tblDanhSachNguoiDung").innerHTML = contentHTML;
};

let renderUserList = () => {
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      showUserInfor(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

renderUserList();
